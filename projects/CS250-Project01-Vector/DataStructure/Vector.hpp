#ifndef _VECTOR_HPP
#define _VECTOR_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

class Vector
{
    private:
    /* Member Variables */
    //! A string pointer used to allocate memory for a dynamic array.
    string* m_data;

    //! Stores how many items have been added to the Vector; less than or equal to the m_arraySize.
    int m_itemCount;

    //! Stores how large the array currently is; update after Resize() is called.
    int m_arraySize;

    public:
    /* Member Functions */
    // Constructor and Destructor
    Vector();
    ~Vector();

    // Deal with end of vector
    void Push( const string& newItem );

    // Deal with middle of vector
    string Get( int index ) const;
    void Remove( int index );

    // Helper functions
    int Size() const;
    bool IsFull() const;
    bool IsEmpty() const;

    private:
    // Internal helper functions
    bool IsInvalidIndex( int index ) const;
    bool IsElementEmpty( int index );

    // Memory mangement
    void AllocateMemory( int newSize = 10 );
    void DeallocateMemory();
    void Resize();

    // Special function, since we haven't covered exceptions yet
    void Panic( string message ) const;
    void NotImplemented() const;

    friend class Tester;
};

/* ****************************************************************************/
/* ************************************************* INITIALIZATION FUNCTIONS */
/* ****************************************************************************/

/**
Set the m_data pointer to nullptr to encourage safe memory management.
Initilaize the m_itemCount and m_arraySize to 0.
*/
//! Called when Vector object is instantiated
Vector::Vector() /*                                                             Vector */
{
    NotImplemented();
}

/**
Call the DeallocateMemory() function to protect against memory leaks.
*/
//! Called when Vector object is destroyed
Vector::~Vector() /*                                                            ~Vector */
{
    NotImplemented();
}

/* ****************************************************************************/
/* ********************************************************* HELPER FUNCTIONS */
/* ****************************************************************************/

/**
    @return bool true if the Vector is empty, false if it is not empty.
*/
//! If the Vector is empty, return true. Otherwise, return false.
bool Vector::IsEmpty() const /*                                                 IsEmpty */
{
    NotImplemented();
	return false; // temporary - delete me
}

/**
    @return bool        true if invalid index (less than 0 or >= m_arraySize),
                        or false if not invalid.
    @param int index    The index to look at.
*/
//! Check to see if a given index is invalid (i.e., negative).
bool Vector::IsInvalidIndex( int index ) const /*                               IsInvalidIndex */
{
    NotImplemented();
	return false; // temporary - delete me
}

/**
    @return bool true if the Vector's array is full, or false if not full.

    Use m_itemCount and m_arraySize to figure out if the array is full
*/
//! If the Vector is full, then return true. Otherwise, return false.
bool Vector::IsFull() const /*                                                  IsFull */
{
    NotImplemented();
	NotImplemented();
	return false; // temporary - delete me
}

/**
    @return int the amount of items stored in the Vector's array.

    Use m_itemCount here.
*/
//! Return the amount of items currently stored in the Vector.
int Vector::Size() const /*                                                     Size */
{
    NotImplemented();
	return -1; // temporary - delete me
}

/**
    @return bool    true if the item at the index is an empty string (""),
                    or false if not.

    Error check: If the index is invalid, call Panic()!
*/
//! Returns whether the element at the given index is empty or not.
bool Vector::IsElementEmpty( int index ) /*                                     IsElementEmpty */
{
    NotImplemented();
	return false; // temporary - delete me
}

/* ****************************************************************************/
/* ********************************************** MEMORY MANAGEMENT FUNCTIONS */
/* ****************************************************************************/

/**
    @return void
    @param int newSize  Defaults to 10 if nothing passed in. Allocates a dynamic
                        array via the m_data pointer.

    If m_data is currently nullptr, then we can allocate memory
    (otherwise exit the function without doing anything)...
    * Set the m_arraySize to the newSize passed in.
    * Set the m_itemCount to 0
    * Allocate an array of size m_arraySize using the m_data pointer.
*/
//! Allocate memory for the dynamic array via the m_data pointer.

void Vector::AllocateMemory( int newSize /* = 10 */ ) /*                        AllocateMemory */
{
    NotImplemented();
}

/**
    @return void

    1. If m_data is already nullptr, nothing needs to be done.
    2. If m_data is NOT nullptr, then...
        * deallocate memory stored at the m_data location.
        * Set m_data to nullptr to prevent invalid memory access.
*/
//! Deallocate memory stored at the address pointed to by the m_data pointer.
void Vector::DeallocateMemory() /*                                              DeallocateMemory */
{
    NotImplemented();
}

/**
    @return void

    Resize() is called when the array is full. It will allocate a bigger array
    in memory, copy all the data from the old array to the new array, and then
    update the m_data pointer. Follow these steps:

    1. Create a new dynamic variable - use a string* pointer,
        and set its new size to the current array size, plus 10.
        This is the "big array."
        (This size is arbitrary; the amount to increase by is a design decision.)
    2. Use a for loop to copy all items from the old (small) array TO the
        new big array.
    3. Afterwards, free up the memory stored at the address pointed to by
        the m_data pointer.
    4. Update the m_data pointer to point at the same location as the "big array" pointer.
    5. Update the old array size to the new size (old size + 10).
*/
//! "Resizes" the dynamic array so that it can hold more items.
void Vector::Resize() /*                                                        Resize */
{
    NotImplemented();
}

/* ****************************************************************************/
/* *********************************************** END-OF-ARRAY FUNCTIONALITY */
/* ****************************************************************************/

/**
    @param const T& newItem - The new item to add to the Vector.
    @return void

    1. If the m_data pointer is currently nullptr, call the AllocateMemory()
        function before continuing.
    2. Otherwise, if the array is full (use IsFull()), call the Resize()
        function before continuing.
    3. After preparing the array (steps 1 and 2), search for an available
        space in the array using a for loop. Once you find an empty spot
        (m_data[i] == ""), then this is the index where you can add the new item.
    4. Add the new item to the array, and increment the m_itemCount.

*/
//! Add a new item to the *end* of the m_data array.
void Vector::Push( const string& newItem ) /*                                   Push */
{
    NotImplemented();
}



/* ****************************************************************************/
/* ************************************************************** ANY ELEMENT */
/* ****************************************************************************/

/**
    @return string      The element value at the index passed in.
    @param int index    The index of the element to return.

    Error check: If the index is invalid, call Panic()!

    Otherwise, return the item at that index from m_data.

*/
//! Returns the element value at the index given.
string Vector::Get( int index ) const /*                                        Get */
{
    NotImplemented();
	return ""; // temporary - delete me
}

/**
    @return void

    Sets the element at the given index to an empty string ("").
    Also decrement m_itemCount.

    Error check: If the index is invalid, call Panic()!
*/
//! Clears out the element in the array at the given index.
void Vector::Remove( int index ) /*                                       Remove */
{
    NotImplemented();
}

/* ****************************************************************************/
/* ************************************************* FUNCTION TO THROW ERRORS */
/* ****************************************************************************/

//! Call this function if something terrible goes wrong.
void Vector::Panic( string message ) const /*                                   Panic */
{
    throw runtime_error( message );
}

//! Marks when a function hasn't been implemented yet.
void Vector::NotImplemented() const /*                                          NotImplemented */
{
    throw runtime_error( "Function not implemented yet!" );
}

#endif
